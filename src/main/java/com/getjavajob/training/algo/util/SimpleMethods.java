package com.getjavajob.training.algo.util;

import java.util.Scanner;

import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 03.03.16.
 */
public class SimpleMethods {
    public static double createDouble() {
        boolean err;
        double x = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextDouble()) {
                x = scan.nextDouble();
            } else {
                out.print("This is not a number, try again ");
                err = true;
            }
        } while (err);
        return x;
    }

    public static int createInteger() {
        boolean err;
        int x = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                x = scan.nextInt();
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return x;
    }

    public static int createTreeDigitalInteger() {
        out.print("Enter a three-digit integer: ");
        boolean err;
        int x = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                x = scan.nextInt();
                if (x < 100 || x > 999) {
                    out.print("It's not tree-digital integer, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return x;
    }

    public static int createHours() {
        out.print("Enter a time in hours: ");
        boolean err;
        int h = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                h = scan.nextInt();
                if (h < 0 || h > 23) {
                    out.print("Condition: 24 > hours >= 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return h;
    }

    public static int createMinutes() {
        out.print("Enter a time in minutes: ");
        boolean err;
        int m = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                m = scan.nextInt();
                if (m < 0 || m > 59) {
                    out.print("Condition: 60 > minutes >= 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return m;
    }

    public static int createSeconds() {
        out.print("Enter a time in seconds: ");
        boolean err;
        int s = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                s = scan.nextInt();
                if (s < 0 || s > 59) {
                    out.print("Condition: 60 > seconds >= 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return s;
    }

    public static int createYear() {
        out.print("Enter the number of year: ");
        boolean err;
        int y = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                y = scan.nextInt();
                if (y < 0) {
                    out.print("Condition: year > 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return y;
    }

    public static int createMonth() {
        out.print("Enter the number of month: ");
        boolean err;
        int m = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                m = scan.nextInt();
                if (m < 1 || m > 12) {
                    out.print("Condition: 13 > month > 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return m;
    }

    public static int createNaturalNumber() {
        out.print("Enter a natural number: ");
        boolean err;
        int n = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                n = scan.nextInt();
                if (n <= 0) {
                    out.print("It's not a natural number (natural number > 0), try again  ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return n;
    }

    public static int createDay() {
        out.print("Enter the number of day in a year: ");
        boolean err;
        int d = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                d = scan.nextInt();
                if (d < 1 || d > 365) {
                    out.print("Condition: 366 > day > 0, try again ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return d;
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + "\t");
        }
    }

    public static void printArray(int[][] array) {
        for (int i[] : array) {
            for (int j : i) {
                System.out.print(j + "\t");
            }
            System.out.println();
        }
    }

    public static String createWord() {
        System.out.print("Enter a word: ");
        boolean err;
        String word;
        do {
            err = false;
            Scanner in = new Scanner(System.in);
            word = in.nextLine();
            if (word.equals("")) {
                out.print("You have not entered the word, try again ");
                err = true;
            }
        } while (err);
        return word;
    }

    public static int[] createArray() {
        System.out.print("Enter a length of the array: ");
        int lengthArray = createNaturalNumber();
        int[] array = new int[lengthArray];
        for (int i = 0; i < lengthArray; i++) {
            System.out.print("Enter element #" + i + " of the array: ");
            array[i] = createInteger();
        }
        return array;
    }

    public static int createNotNegativeInteger() {
        boolean err;
        int n = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                n = scan.nextInt();
                if (n < 0) {
                    out.print("It's negative number, try again  ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return n;
    }

    public static int createNumberOfNumeralSystem() {
        out.print("Enter a number of the numeral system (s). Condition: 2 <= s <= 16: ");
        boolean err;
        int n = 0;
        do {
            err = false;
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextInt()) {
                n = scan.nextInt();
                if (n < 2 || n > 16) {
                    out.print("It's not a a number of the numeral system, try again  ");
                    err = true;
                }
            } else {
                out.print("It's not an integer, try again ");
                err = true;
            }
        } while (err);
        return n;
    }

    public static void printArray(double[] array) {
        for (double i : array) {
            System.out.println(i);
        }
    }

    public static int[][] createArray(int lines, int columns) {
        int[][] array = new int[lines][columns];
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            System.out.println("Line #" + (i + 1));
            for (int j = 0; j < array[i].length; j++) {
                System.out.print("Column #" + (j + 1) + ": ");
                array[i][j] = in.nextInt();
            }
        }
        return array;
    }

    public static int createPointsBasketball() {
        boolean err;
        int points;
        do {
            err = false;
            points = createInteger();
            if (points != 0 && points != 1 && points != 2 && points != 3) {
                out.print("You can only enter: 0 (end the game) or 1 or 2 or 3, try again ");
                err = true;
            }
        } while (err);
        return points;
    }

    public static int createTeamNumber() {
        boolean err;
        int teamNumber;
        do {
            err = false;
            teamNumber = createInteger();
            if (teamNumber != 1 && teamNumber != 2) {
                out.print("You can only enter: 1 or 2, try again ");
                err = true;
            }
        } while (err);
        return teamNumber;
    }
}
