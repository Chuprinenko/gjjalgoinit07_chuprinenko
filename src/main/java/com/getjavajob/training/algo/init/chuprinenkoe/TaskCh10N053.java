package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 15.02.16.
 */
public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] mas = createArray();
        int[] mas2 = new int[mas.length];
        printArray(mas);
        System.out.println();
        printArray(copyArrayInReversOrder(mas, mas2, 0));
    }

    public static int[] copyArrayInReversOrder(int[] x, int[] y, int indexOfInitialElement) {
        if (x.length - indexOfInitialElement == 0) {
            return y;
        }
        y[indexOfInitialElement] = x[x.length - indexOfInitialElement - 1];
        copyArrayInReversOrder(x, y, ++indexOfInitialElement);
        return y;
    }
}