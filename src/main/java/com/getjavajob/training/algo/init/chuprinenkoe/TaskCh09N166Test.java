package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N166.changeFirstAndLastWordsInSentence;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 10.02.16.
 */
public class TaskCh09N166Test {
    public static void main(String[] args) {
        test1ChangeFirstAndLastWordsInSentence();
        test2ChangeFirstAndLastWordsInSentence();
        test3ChangeFirstAndLastWordsInSentence();
    }

    public static void test1ChangeFirstAndLastWordsInSentence() {
        assertEquals("TaskCh09N166Test.test1ChangeFirstAndLastWordsInSentence", "you love I.",
                changeFirstAndLastWordsInSentence("I love you."));
    }

    public static void test2ChangeFirstAndLastWordsInSentence() {
        assertEquals("TaskCh09N166Test.test2ChangeFirstAndLastWordsInSentence", "you love I!",
                changeFirstAndLastWordsInSentence("I love you!"));
    }

    public static void test3ChangeFirstAndLastWordsInSentence() {
        assertEquals("TaskCh09N166Test.test3ChangeFirstAndLastWordsInSentence", "me you love Do?",
                changeFirstAndLastWordsInSentence("Do you love me?"));
    }
}
