package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N015.calculateAgeInYears;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenko on 02.02.16.
 */
public class TaskCh04N015Test {
    public static void main(String[] args) {
        test1CalculateAgeInYears();
        test2CalculateAgeInYears();
        test3CalculateAgeInYears();
    }

    public static void test1CalculateAgeInYears() {
        assertEquals("TaskCh04N015Test.test1CalculateAgeInYears", 29,
                calculateAgeInYears(6, 1985, 12, 2014));
    }

    public static void test2CalculateAgeInYears() {
        assertEquals("TaskCh04N015Test.test2CalculateAgeInYears", 28,
                calculateAgeInYears(6, 1985, 5, 2014));
    }

    public static void test3CalculateAgeInYears() {
        assertEquals("TaskCh04N015Test.test3CalculateAgeInYears", 29,
                calculateAgeInYears(6, 1985, 6, 2014));
    }
}
