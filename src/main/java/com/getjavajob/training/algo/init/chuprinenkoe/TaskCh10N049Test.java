package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N049.calculateIndexOfMaxElementInArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N049Test {
    public static void main(String[] args) {
        testIndexOfMaxElementInArray();
    }

    public static void testIndexOfMaxElementInArray() {
        int[] array = {1, 2, 3, 4, 5, 6, 5, 4, 3, 2};
        assertEquals("TaskCh10N049Test.testIndexOfMaxElementInArray", 5, calculateIndexOfMaxElementInArray(array, 0));
    }
}
