package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;

/**
 * Created by chuprinenkoe on 20.01.16.
 */
public class TaskCh01N003 {
    public static void main(String[] args) {
        System.out.print("Enter a number: ");
        System.out.print(createResult(createDouble()));
    }

    public static String createResult(double n) {
        return "You entered the number " + n;
    }
}
