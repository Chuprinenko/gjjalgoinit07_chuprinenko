package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh11N245.createArrayFirstNegativeThenRest;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 18.02.16.
 */
public class TaskCh11N245Test {
    public static void main(String[] args) {
        testArrayFirstNegativeThenRest();
    }

    public static void testArrayFirstNegativeThenRest() {
        int[] array = {-1, 0, -1, 0, -1, 0, -1, 0, -1, 0};
        int[] newArray = {-1, -1, -1, -1, -1, 0, 0, 0, 0, 0};
        assertEquals("TaskCh11N245Test.testArrayFirstNegativeThenRest", newArray,
                createArrayFirstNegativeThenRest(array));
    }
}
