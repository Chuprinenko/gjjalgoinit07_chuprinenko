package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh02N013.createInvertedTreeDigitalInteger;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 28.01.16.
 */
public class TaskCh02N013Test {
    public static void main(String[] args) {
        testCreateInvertedTreeDigitalInteger();
    }

    public static void testCreateInvertedTreeDigitalInteger() {
        assertEquals("TaskCh02N013Test.testInvertedTreeDigitalInteger",
                521, createInvertedTreeDigitalInteger(125));
    }
}
