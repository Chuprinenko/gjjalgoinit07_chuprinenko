package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createTreeDigitalInteger;

/**
 * Created by chuprinenkoe on 29.01.16.
 */
public class TaskCh02N031 {
    public static void main(String[] args) {
        int result = swapSecondAndThirdDigitsInThreeDigitalInteger(createTreeDigitalInteger());
        System.out.print("Integer which reversed the second and third digits: " + result);
    }

    public static int swapSecondAndThirdDigitsInThreeDigitalInteger(int a) {
        return a / 100 * 100 + a % 10 * 10 + a / 10 % 10;
    }
}
