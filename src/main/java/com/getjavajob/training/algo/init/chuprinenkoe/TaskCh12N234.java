package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 27.02.16.
 */
public class TaskCh12N234 {
    public static void main(String[] args) {
        int[][] array = createArray(4, 4);
        printArray(array);
        deleteColumn(array, 2);
        System.out.println();
        printArray(array);
        System.out.println();
        deleteRow(array, 2);
        printArray(array);
    }

    public static int[][] deleteColumn(int[][] array, int numberColumn) {
        for (int i = 0; i < array.length; i++) {
            System.arraycopy(array[i], numberColumn + 1, array[i], numberColumn, array[i].length - numberColumn - 1);
            array[i][array[i].length - 1] = 0;
        }
        return array;
    }

    public static int[][] deleteRow(int[][] array, int numberRow) {
        for (int i = numberRow; i < array.length - 1; i++) {
            System.arraycopy(array[i + 1], 0, array[i], 0, array[i].length);
        }
        for (int j = 0; j < array[0].length; j++) {
            array[array.length - 1][j] = 0;
        }
        return array;
    }
}
