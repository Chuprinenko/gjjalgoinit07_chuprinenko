package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N036.calculateColorTrafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N036Test {
    public static void main(String[] args) {
        testColorTrafficLightRed();
        testColorTrafficLightGreen();
    }

    public static void testColorTrafficLightRed() {
        assertEquals("TaskCh04N033Test.testColorTrafficLightRed", "red", calculateColorTrafficLight(3));
    }

    public static void testColorTrafficLightGreen() {
        assertEquals("TaskCh04N033Test.testColorTrafficLightGreen", "green", calculateColorTrafficLight(5));
    }
}
