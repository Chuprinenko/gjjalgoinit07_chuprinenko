package com.getjavajob.training.algo.init.chuprinenkoe;

import java.util.Scanner;

/**
 * Created by chuprinenkoe on 10.02.16.
 */
public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter а sentence: ");
        System.out.print(changeFirstAndLastWordsInSentence(in.nextLine()));
    }

    public static String changeFirstAndLastWordsInSentence(String in) {
        String arraySentence[] = in.split(" ");
        String firstWord = arraySentence[0];
        arraySentence[0] = arraySentence[arraySentence.length - 1];
        arraySentence[arraySentence.length - 1] = firstWord;
        StringBuilder newSentence = new StringBuilder("");
        for (int i = 0; i < arraySentence.length; i++) {
            if (i != arraySentence.length - 1) {
                newSentence = newSentence.append(arraySentence[i]).append(" ");
            } else {
                newSentence = newSentence.append(arraySentence[i]);
            }
        }
        if (in.indexOf('.') != -1) {
            int dot = newSentence.indexOf(".");
            newSentence.deleteCharAt(dot);
            newSentence = newSentence.append(".");
        }
        if (in.indexOf('!') != -1) {
            int exclamation = newSentence.indexOf("!");
            newSentence.deleteCharAt(exclamation);
            newSentence = newSentence.append("!");
        }
        if (in.indexOf('?') != -1) {
            int question = newSentence.indexOf("?");
            newSentence.deleteCharAt(question);
            newSentence = newSentence.append("?");
        }
        return newSentence.toString();
    }
}