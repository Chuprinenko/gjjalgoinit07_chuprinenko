package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;
import static java.lang.Math.sqrt;

/**
 * Created by chuprinenkoe on 16.02.16.
 */
public class TaskCh10N056 {
    public static void main(String[] args) {
        int n = createNaturalNumber();
        int firstDivisor = 2;
        System.out.print("Number " + n + " is prime number: " + isPrimeNumber(n, firstDivisor));
    }

    public static boolean isPrimeNumber(int number, int firstDivisor) {
        if (number == 2) {
            return true;
        } else if (number == 1 || number % firstDivisor == 0) {
            return false;
        }
        return firstDivisor >= sqrt(number) || isPrimeNumber(number, ++firstDivisor);
    }
}
