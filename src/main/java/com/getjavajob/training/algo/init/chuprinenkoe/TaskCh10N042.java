package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;
import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        System.out.print("Enter a number: ");
        double a = createDouble();
        System.out.print("Power of number - ");
        int n = createNaturalNumber();
        System.out.print(a + " in power " + n + " is " + calculatePowerOfNumber(a, n));
    }

    public static double calculatePowerOfNumber(double a, int n) {
        if (n == 1) {
            return a;
        }
        return calculatePowerOfNumber(a, n - 1) * a;
    }
}
