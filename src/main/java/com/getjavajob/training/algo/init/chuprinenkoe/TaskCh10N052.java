package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 15.02.16.
 */
public class TaskCh10N052 {
    public static void main(String[] args) {
        int n = createNaturalNumber();
        StringBuilder a = new StringBuilder();
        System.out.print("Number: " + n + ", Reverse number: " + createReverseNumber(a, n));
    }

    public static String createReverseNumber(StringBuilder a, int n) {
        if (n >= 1) {
            a.append(n % 10);
            createReverseNumber(a, n / 10);
        }
        return a.toString();
    }
}