package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.println("The digital root is " + calculateDigitalRoot(createNaturalNumber()));
    }

    public static int calculateDigitalRoot(int n) {
        if (n < 10) {
            return n;
        }
        return calculateDigitalRoot(calculateDigitalRoot(n / 10) + n % 10);
    }
}