package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;
import static java.lang.Math.pow;

/**
 * Created by chuprinenkoe on 05.02.16.
 */
public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.print("Enter the number of limiting sequence: ");
        printArray(createLimitedSequence(createInteger()));
    }

    public static int[] createLimitedSequence(int limitingNumber) {
        int arrayLength = (int) Math.sqrt(limitingNumber);
        int[] limitedSequence = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            limitedSequence[i] = (int) pow(i + 1, 2);
        }
        return limitedSequence;
    }
}
