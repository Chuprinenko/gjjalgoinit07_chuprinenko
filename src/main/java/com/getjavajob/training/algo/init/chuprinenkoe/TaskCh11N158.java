package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 17.02.16.
 */
public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] array = createArray();
        printArray(array);
        System.out.println();
        printArray(removeDuplicateElementsFromArray(array));
    }

    public static int[] removeDuplicateElementsFromArray(int[] array) {
        int[] newArray = new int[array.length];
        int next = 0;
        for (int i = 0; i < array.length; i++) {
            int repeat = 0;
            for (int j = 0; j <= i; j++) {
                if (array[j] == array[i]) {
                    repeat++;
                }
            }
            if (repeat == 1) {
                newArray[next] = array[i];
                next++;
            }
        }
        System.arraycopy(newArray, 0, array, 0, array.length);
        return array;
    }
}
