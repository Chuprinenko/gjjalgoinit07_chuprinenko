package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 24.02.16.
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        System.out.println("Array A");
        int[][] arrayA = createArrayTaskA(6, 6);
        printArray(arrayA);
        System.out.println("Array B");
        int[][] arrayB = createArrayTaskB(6, 6);
        printArray(arrayB);
    }

    public static int[][] createArrayTaskA(int lines, int columns) {
        int[][] array = new int[lines][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == 0 || j == 0) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = array[i][j - 1] + array[i - 1][j];
                }
            }
        }
        return array;
    }

    public static int[][] createArrayTaskB(int lines, int columns) {
        int[][] array = new int[lines][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (i + j) % array.length + 1;
            }
        }
        return array;
    }
}
