package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createMonth;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N106 {
    public static void main(String[] args) {
        int month = createMonth();
        System.out.print("Month #" + month + " is " + calculateSeason(month));
    }

    public static String calculateSeason(int month) {
        String season = "";
        switch (month) {
            case 1:
            case 2:
            case 12:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
        }
        return season;
    }
}
