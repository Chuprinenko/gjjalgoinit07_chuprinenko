package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N056.isPrimeNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 16.02.16.
 */
public class TaskCh10N056Test {
    public static void main(String[] args) {
        test1PrimeNumber();
        test2PrimeNumber();
        test3PrimeNumber();
        test4PrimeNumber();
    }

    public static void test1PrimeNumber() {
        assertEquals("TaskCh10N056Test.test1Prime", false, isPrimeNumber(1, 2));
    }

    public static void test2PrimeNumber() {
        assertEquals("TaskCh10N056Test.test2Prime", true, isPrimeNumber(2, 2));
    }

    public static void test3PrimeNumber() {
        assertEquals("TaskCh10N056Test.test3Prime", false, isPrimeNumber(4, 2));
    }

    public static void test4PrimeNumber() {
        assertEquals("TaskCh10N056Test.test4Prime", true, isPrimeNumber(137, 2));
    }
}
