package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createWord;

/**
 * Created by chuprinenkoe on 09.02.16.
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        System.out.print(changeFirstAToLastO(createWord()));
    }

    public static String changeFirstAToLastO(String word) {
        StringBuilder newWord = new StringBuilder(word);
        int aInd = word.toLowerCase().indexOf('a');
        int oInd = word.toLowerCase().lastIndexOf('o');
        if (aInd != -1 && oInd != -1) {
            newWord.setCharAt(aInd, word.charAt(oInd));
            newWord.setCharAt(oInd, word.charAt(aInd));
        }
        return newWord.toString();
    }
}