package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N024.createArrayTaskA;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N024.createArrayTaskB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 24.02.16.
 */
public class TaskCh12N024Test {
    public static void main(String[] args) {
        testCreateArrayTaskA();
        testCreateArrayTaskB();
    }

    public static void testCreateArrayTaskA() {
        int[][] array = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testCreateArrayTaskA", array, createArrayTaskA(6, 6));
    }

    public static void testCreateArrayTaskB() {
        int[][] array = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.testCreateArrayTaskB", array, createArrayTaskB(6, 6));
    }
}
