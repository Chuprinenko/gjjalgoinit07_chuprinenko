package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;
import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static java.lang.Math.pow;
import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        out.print("Enter the initial term of a geometric progression (a1). Condition a1!=0: ");
        double a1 = createDouble();
        out.print("Enter the common ratio (r). Condition r!=0: ");
        double r = createDouble();
        out.print("Enter the number of term of the sequence (n): ");
        int n = createInteger();
        out.println("Term #" + n + " of the geometric progression is " + calculateTermOfGP(a1, r, n));
        out.println("The sum of a finite (" + n + ") geometric progression is " + calculateSumOfGP(a1, r, n));
    }

    public static double calculateTermOfGP(double initialTerm, double ratio, int numberOfTerm) {
        if (numberOfTerm == 1) {
            return initialTerm;
        }
        return calculateTermOfGP(initialTerm * ratio, ratio, numberOfTerm - 1);
    }

    public static double calculateSumOfGP(double initialTerm, double ratio, int numberOfTerm) {
        if (numberOfTerm == 1) {
            return initialTerm;
        }
        return calculateSumOfGP(initialTerm, ratio, numberOfTerm - 1) + initialTerm * pow(ratio, numberOfTerm - 1);
    }
}
