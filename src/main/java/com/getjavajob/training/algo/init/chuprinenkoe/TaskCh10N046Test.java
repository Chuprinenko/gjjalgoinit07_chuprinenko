package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N046.calculateSumOfGP;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N046.calculateTermOfGP;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N046Test {
    public static void main(String[] args) {
        testTermOfGP();
        testSumOfGP();
    }

    public static void testTermOfGP() {
        assertEquals("TaskCh10N046Test.testTermOfGP", 32, calculateTermOfGP(2, 2, 5));
    }

    public static void testSumOfGP() {
        assertEquals("TaskCh10N046Test.testSumOfGP", 62, calculateSumOfGP(2, 2, 5));
    }
}
