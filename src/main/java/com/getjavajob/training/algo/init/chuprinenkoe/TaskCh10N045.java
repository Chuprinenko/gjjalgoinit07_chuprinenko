package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;
import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.print("Enter the initial term of an arithmetic progression (a1): ");
        double a1 = createDouble();
        System.out.print("Enter the common difference of successive members (d): ");
        double d = createDouble();
        System.out.print("Enter the number of term of the sequence (n): ");
        int n = createInteger();
        System.out.println("Term #" + n + " of the sequence is " + calculateTermOfAP(a1, d, n));
        System.out.println("The sum of a finite (" + n + ") arithmetic progression is " + calculateSumOfAP(a1, d, n));
    }

    public static double calculateTermOfAP(double initialTerm, double difference, int numberOfTerm) {
        if (numberOfTerm == 1) {
            return initialTerm;
        }
        return calculateTermOfAP(initialTerm + difference, difference, numberOfTerm - 1);
    }

    public static double calculateSumOfAP(double initialTerm, double difference, int numberOfTerm) {
        if (numberOfTerm == 1) {
            return initialTerm;
        }
        return calculateSumOfAP(initialTerm, difference, numberOfTerm - 1) + difference * (numberOfTerm - 1) +
                initialTerm;
    }
}
