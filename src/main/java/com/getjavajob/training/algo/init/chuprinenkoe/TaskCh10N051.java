package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 15.02.16.
 */
public class TaskCh10N051 {
    public static void main(String[] args) {
        System.out.print("Enter a positive (n): ");
        int n = createNaturalNumber();
        StringBuilder a = new StringBuilder();
        StringBuilder b = new StringBuilder();
        StringBuilder c = new StringBuilder();
        System.out.println("Task A: " + createTaskA(a, n));
        System.out.println("Task B: " + createTaskB(b, n));
        System.out.println("Task C: " + createTaskC(c, n));
    }

    public static String createTaskA(StringBuilder a, int n) {
        if (n > 0) {
            a.append(n);
            createTaskA(a, n - 1);
        }
        return a.toString();
    }

    public static String createTaskB(StringBuilder a, int n) {
        if (n > 0) {
            createTaskB(a, n - 1);
            a.append(n);
        }
        return a.toString();
    }

    public static String createTaskC(StringBuilder a, int n) {
        if (n > 0) {
            a.append(n);
            createTaskC(a, n - 1);
            a.append(n);
        }
        return a.toString();
    }
}
