package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh02N043.checkDivision;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 29.01.16.
 */
public class TaskCh02N043Test {
    public static void main(String[] args) {
        testCheckDivisionTrue();
        testCheckDivisionFalse();
    }

    public static void testCheckDivisionTrue() {
        assertEquals("TaskCh02N043Test.testCheckDivisionTrue", 1, checkDivision(27, 3));
    }

    public static void testCheckDivisionFalse() {
        assertEquals("TaskCh02N043Test.testCheckDivisionFalse", 29, checkDivision(7, 11));
    }
}
