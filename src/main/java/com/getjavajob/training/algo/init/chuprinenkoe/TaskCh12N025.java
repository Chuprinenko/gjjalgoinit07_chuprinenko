package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 24.02.16.
 */
public class TaskCh12N025 {
    public static void main(String[] args) {
        System.out.println("Array A");
        int[][] arrayA = createArrayTaskA(12, 10);
        printArray(arrayA);
        System.out.println("Array B");
        int[][] arrayB = createArrayTaskB(12, 10);
        printArray(arrayB);
        System.out.println("Array C");
        int[][] arrayC = createArrayTaskC(12, 10);
        printArray(arrayC);
        System.out.println("Array D");
        int[][] arrayD = createArrayTaskD(12, 10);
        printArray(arrayD);
        System.out.println("Array E");
        int[][] arrayE = createArrayTaskE(10, 12);
        printArray(arrayE);
        System.out.println("Array F");
        int[][] arrayF = createArrayTaskF(12, 10);
        printArray(arrayF);
        System.out.println("Array G");
        int[][] arrayG = createArrayTaskG(12, 10);
        printArray(arrayG);
        System.out.println("Array H");
        int[][] arrayH = createArrayTaskH(12, 10);
        printArray(arrayH);
        System.out.println("Array I");
        int[][] arrayI = createArrayTaskI(12, 10);
        printArray(arrayI);
        System.out.println("Array J");
        int[][] arrayJ = createArrayTaskJ(12, 10);
        printArray(arrayJ);
        System.out.println("Array K");
        int[][] arrayK = createArrayTaskK(12, 10);
        printArray(arrayK);
        System.out.println("Array L");
        int[][] arrayL = createArrayTaskL(12, 10);
        printArray(arrayL);
        System.out.println("Array M");
        int[][] arrayM = createArrayTaskM(12, 10);
        printArray(arrayM);
        System.out.println("Array N");
        int[][] arrayN = createArrayTaskN(12, 10);
        printArray(arrayN);
        System.out.println("Array O");
        int[][] arrayO = createArrayTaskO(12, 10);
        printArray(arrayO);
        System.out.println("Array P");
        int[][] arrayP = createArrayTaskP(12, 10);
        printArray(arrayP);
    }

    public static int[][] createArrayTaskA(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskB(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int j = 0; j < array[0].length; j++) {
            for (int i = 0; i < array.length; i++) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskC(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = array[i].length - 1; j >= 0; j--) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskD(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int j = 0; j < array[0].length; j++) {
            for (int i = array.length - 1; i >= 0; i--) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskE(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int i = 0; i < array.length; i++) {
            if (route % 2 == 1) {
                for (int j = 0; j < array[i].length; j++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int j = array[i].length - 1; j >= 0; j--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskF(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int j = 0; j < array[0].length; j++) {
            if (route % 2 == 1) {
                for (int i = 0; i < array.length; i++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int i = array.length - 1; i >= 0; i--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskG(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskH(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int j = array[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < array.length; i++) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskI(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = array[i].length - 1; j >= 0; j--) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskJ(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        for (int j = array[0].length - 1; j >= 0; j--) {
            for (int i = array.length - 1; i >= 0; i--) {
                array[i][j] = start + temp;
                temp += step;
            }
        }
        return array;
    }

    public static int[][] createArrayTaskK(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int i = array.length - 1; i >= 0; i--) {
            if (route % 2 == 1) {
                for (int j = 0; j < array[i].length; j++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int j = array[i].length - 1; j >= 0; j--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskL(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int i = 0; i < array.length; i++) {
            if (route % 2 == 1) {
                for (int j = array[i].length - 1; j >= 0; j--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int j = 0; j < array[i].length; j++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskM(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int j = array[0].length - 1; j >= 0; j--) {
            if (route % 2 == 1) {
                for (int i = 0; i < array.length; i++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int i = array.length - 1; i >= 0; i--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskN(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int j = 0; j < array[0].length; j++) {
            if (route % 2 == 1) {
                for (int i = array.length - 1; i >= 0; i--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int i = 0; i < array.length; i++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskO(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int start = 1;
        int step = 1;
        int temp = 0;
        int route = 1;
        for (int i = array.length - 1; i >= 0; i--) {
            if (route % 2 == 1) {
                for (int j = array[i].length - 1; j >= 0; j--) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            } else {
                for (int j = 0; j < array[i].length; j++) {
                    array[i][j] = start + temp;
                    temp += step;
                }
            }
            route++;
        }
        return array;
    }

    public static int[][] createArrayTaskP(int lines, int columns) {
        int[][] array = new int[lines][columns];
        int arrayVolume = lines * columns;
        int lineStart = lines - 1;
        int columnStart = columns - 1;
        int lineDirection = -1;
        int steps = 1;
        for (int i = 1; i <= arrayVolume; i++) {
            array[lineStart][columnStart] = steps;
            if (steps % lines == 0) {
                columnStart--;
                lineDirection = -lineDirection;
                lineStart -= lineDirection;
            }
            lineStart += lineDirection;
            steps++;
        }
        return array;
    }
}
