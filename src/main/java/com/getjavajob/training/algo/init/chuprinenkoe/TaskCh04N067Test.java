package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N067.calculateWhatDay;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N067Test {
    public static void main(String[] args) {
        testWhatDayWorkday();
        testWhatDayWeekend();
    }

    public static void testWhatDayWorkday() {
        assertEquals("TaskCh04N067Test.testWhatDayWorkday", "Workday", calculateWhatDay(5));
    }

    public static void testWhatDayWeekend() {
        assertEquals("TaskCh04N067Test.testWhatDayWeekend", "Weekend", calculateWhatDay(7));
    }
}
