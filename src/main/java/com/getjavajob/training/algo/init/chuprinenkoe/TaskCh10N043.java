package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        int number = createNaturalNumber();
        System.out.println("The sum of digits is " + calculateSumOfDigits(number));
        System.out.println("Amount of digits is " + calculateAmountOfDigits(number));
    }

    public static int calculateSumOfDigits(int n) {
        if (n < 10) {
            return n;
        }
        return calculateSumOfDigits(n / 10) + n % 10;
    }

    public static int calculateAmountOfDigits(int n) {
        if (n < 10) {
            return 1;
        }
        return calculateAmountOfDigits(n / 10) + 1;
    }
}