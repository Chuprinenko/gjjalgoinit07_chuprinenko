package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N041.calculateFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N041Test {
    public static void main(String[] args) {
        testFactorial();
    }

    public static void testFactorial() {
        assertEquals("TaskCh10N041Test.testFactorial", 120, calculateFactorial(5));
    }
}
