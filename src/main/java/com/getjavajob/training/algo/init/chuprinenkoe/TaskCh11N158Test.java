package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh11N158.removeDuplicateElementsFromArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 18.02.16.
 */
public class TaskCh11N158Test {
    public static void main(String[] args) {
        testRemoveDuplicateElementsFromArray();
    }

    public static void testRemoveDuplicateElementsFromArray() {
        int[] array = {0, 0, 0, 1, 1, 0, 4, 0, 2, 2};
        int[] newArray = {0, 1, 4, 2, 0, 0, 0, 0, 0, 0};
        assertEquals("TaskCh11N158Test.testRemoveDuplicateElementsFromArray", newArray,
                removeDuplicateElementsFromArray(array));
    }
}
