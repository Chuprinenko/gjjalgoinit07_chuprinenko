package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;
import static com.getjavajob.training.algo.util.SimpleMethods.createNumberOfNumeralSystem;

/**
 * Created by chuprinenkoe on 16.02.16.
 */
public class TaskCh10N055 {
    public static void main(String[] args) {
        System.out.print("Enter an original number (n): ");
        int n = createNaturalNumber();
        int s = createNumberOfNumeralSystem();
        StringBuilder result = new StringBuilder();
        System.out.print("Number " + n + " in (" + s + ") numeral system: "
                + conversionIntegerInAnotherNumeralSystem(n, s, result));
    }

    public static String conversionIntegerInAnotherNumeralSystem(int n, int s, StringBuilder result) {
        char[] tab = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        if (n == 0) {
            return result.toString();
        }
        conversionIntegerInAnotherNumeralSystem(n / s, s, result);
        result.append(tab[n % s]);
        return result.toString();
    }
}