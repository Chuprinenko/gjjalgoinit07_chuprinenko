package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N047.calculateTermOfFibonacciSequence;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N047Test {
    public static void main(String[] args) {
        testTermOfFibonacciSequence();
    }

    public static void testTermOfFibonacciSequence() {
        assertEquals("TaskCh10N047Test.testTermOfFibonacciSequence", 21, calculateTermOfFibonacciSequence(8));
    }
}
