package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 27.02.16.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] school = createSchoolList(11, 4);
        printArray(school);
        printArray(calculateAverageNumberOfStudentsAtEachLevel(school));
    }

    public static double[] calculateAverageNumberOfStudentsAtEachLevel(int[][] school) {
        double[] array = new double[school.length];
        int j;
        for (int i = 0; i < school.length; i++) {
            double temp = 0;
            for (j = 0; j < school[i].length; j++) {
                temp += school[i][j];
            }
            array[i] = temp / j;
        }
        return array;
    }

    public static int[][] createSchoolList(int levels, int classes) {
        int[][] array = new int[levels][classes];
        for (int i = 0; i < array.length; i++) {
            System.out.println((i + 1) + " Level. Enter the number of students in classes");
            for (int j = 0; j < array[i].length; j++) {
                System.out.print("Class #" + (j + 1) + ": ");
                array[i][j] = createInteger();
            }
        }
        return array;
    }
}
