package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N052.createReverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 15.02.16.
 */
public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverseNumber();
    }

    static public void testReverseNumber() {
        StringBuilder a = new StringBuilder();
        assertEquals("TestCh10N052Test.testReverseNumber", "54321", createReverseNumber(a, 12345));
    }
}
