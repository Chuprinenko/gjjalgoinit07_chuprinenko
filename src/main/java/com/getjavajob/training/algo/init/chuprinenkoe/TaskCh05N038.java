package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;

/**
 * Created by chuprinenkoe on 04.02.16.
 */
public class TaskCh05N038 {
    public static void main(String[] args) {
        System.out.print("Enter a distance from work to home in km: ");
        double distanceFromWorkToHome = createDouble();
        System.out.println(calculateDistanceFromHome(distanceFromWorkToHome, 100));
        System.out.println(calculateTotalDistanceTraveled(distanceFromWorkToHome, 100));
    }

    public static double calculateDistanceFromHome(double distanceFromWorkToHome, int numberOfStages) {
        double distanceFromHome = 0;
        double distanceToHome = 0;
        for (int i = 1; i <= numberOfStages; i += 2) {
            distanceFromHome += distanceFromWorkToHome / i;
        }
        for (int i = 2; i <= numberOfStages; i += 2) {
            distanceToHome += distanceFromWorkToHome / i;
        }
        return distanceFromHome - distanceToHome;
    }

    public static double calculateTotalDistanceTraveled(double distanceFromWorkToHome, int numberOfStages) {
        double totalDistanceTraveled = 0;
        for (int i = 1; i <= numberOfStages; i++) {
            totalDistanceTraveled += distanceFromWorkToHome / i;
        }
        return totalDistanceTraveled;
    }
}
