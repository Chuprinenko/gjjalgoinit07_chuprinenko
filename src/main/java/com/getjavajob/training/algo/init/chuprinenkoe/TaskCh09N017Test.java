package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N017.isSameFirstAndLastLetters;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N017Test {
    public static void main(String[] args) {
        testSameFirstAndLastLettersTrue();
        testSameFirstAndLastLettersFalse();
    }

    public static void testSameFirstAndLastLettersTrue() {
        assertEquals("TaskCh09N017Test.testSameFirstAndLastLettersTrue", true, isSameFirstAndLastLetters("Delighted"));
    }

    public static void testSameFirstAndLastLettersFalse() {
        assertEquals("TaskCh09N017Test.testSameFirstAndLastLettersFalse", false, isSameFirstAndLastLetters("Mama"));
    }
}
