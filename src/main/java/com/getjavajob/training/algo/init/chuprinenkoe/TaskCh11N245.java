package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 18.02.16.
 */
public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] array = createArray();
        printArray(array);
        System.out.println();
        printArray(createArrayFirstNegativeThenRest(array));
    }

    public static int[] createArrayFirstNegativeThenRest(int[] array) {
        int[] newArray = new int[array.length];
        int negative = 0;
        int rest = 1;
        for (int x : array) {
            if (x < 0) {
                newArray[negative] = x;
                negative++;
            } else {
                newArray[array.length - rest] = x;
                rest++;
            }
        }
        return newArray;
    }
}
