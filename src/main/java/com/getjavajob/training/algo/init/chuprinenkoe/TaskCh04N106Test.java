package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N106.calculateSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N106Test {
    public static void main(String[] args) {
        testSeasonSummer();
        testSeasonAutumn();
        testSeasonWinter();
        testSeasonSpring();
    }

    public static void testSeasonSummer() {
        assertEquals("TaskCh04N106Test.testSeasonSummer", "Summer", calculateSeason(8));
    }

    public static void testSeasonAutumn() {
        assertEquals("TaskCh04N106Test.testSeasonAutumn", "Autumn", calculateSeason(11));
    }

    public static void testSeasonWinter() {
        assertEquals("TaskCh04N106Test.testSeasonWinter", "Winter", calculateSeason(2));
    }

    public static void testSeasonSpring() {
        assertEquals("TaskCh04N106Test.testSeasonSpring", "Spring", calculateSeason(5));
    }
}
