package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;

/**
 * Created by chuprinenkoe on 29.01.16.
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.print("Enter an  integer variable a: ");
        int a = createInteger();
        System.out.print("Enter an  integer variable b: ");
        int b = createInteger();
        System.out.print(checkDivision(a, b));
    }

    public static int checkDivision(int a, int b) {
        return 1 + (a % b) * (b % a);
    }
}
