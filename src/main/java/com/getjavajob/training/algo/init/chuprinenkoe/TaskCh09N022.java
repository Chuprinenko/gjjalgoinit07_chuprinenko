package com.getjavajob.training.algo.init.chuprinenkoe;

import java.util.Scanner;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        String word = createWordWithEvenNumberOfLetters();
        System.out.print(takeFirstHalfWord(word));
    }

    public static String takeFirstHalfWord(String word) {
        return word.substring(0, word.length() / 2);
    }

    public static String createWordWithEvenNumberOfLetters() {
        boolean err;
        String word;
        do {
            err = false;
            Scanner in = new Scanner(System.in);
            System.out.print("Enter a word consisting of an even number of letters: ");
            word = in.nextLine();
            if (word.length() % 2 != 0 || word.length() == 0) {
                System.out.println("The quantity of letters in a word must be even.");
                err = true;
            }
        } while (err);
        return word;
    }
}
