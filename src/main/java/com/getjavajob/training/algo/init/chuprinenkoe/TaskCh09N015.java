package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static com.getjavajob.training.algo.util.SimpleMethods.createWord;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N015 {
    public static void main(String[] args) {
        String word = createWord();
        int charIndex = createCharacterIndex(word);
        printCharacterFromWord(word, charIndex);
    }

    public static char takeCharacterFromWord(String word, int charIndex) {
        return word.charAt(charIndex);
    }

    public static void printCharacterFromWord(String word, int charIndex) {
        System.out.print("Character #" + charIndex + ": " + word.charAt(charIndex));
    }

    public static int createCharacterIndex(String word) {
        boolean err;
        int charIndex;
        do {
            err = false;
            System.out.print("Enter the number of the character (from 0 to " + (word.length() - 1) + "): ");
            charIndex = createInteger();
            if (charIndex < 0 || charIndex >= word.length()) {
                System.out.println("You entered wrong number of the character");
                err = true;
            }
        } while (err);
        return charIndex;
    }
}
