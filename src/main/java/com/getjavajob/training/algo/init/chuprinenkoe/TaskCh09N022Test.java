package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N022.takeFirstHalfWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N022Test {
    public static void main(String[] args) {
        testFirstHalfWord();
    }

    public static void testFirstHalfWord() {
        assertEquals("TaskCh09N022Test.testFirstHalfWord", "lik", takeFirstHalfWord("likely"));
    }
}
