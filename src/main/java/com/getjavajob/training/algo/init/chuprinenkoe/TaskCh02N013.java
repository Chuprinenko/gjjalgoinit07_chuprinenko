package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createTreeDigitalInteger;

/**
 * Created by chuprinenkoe on 28.01.16.
 */
public class TaskCh02N013 {
    public static void main(String[] args) {
        int inverted = createInvertedTreeDigitalInteger(createTreeDigitalInteger());
        System.out.print("Inverted integer: " + inverted);
    }

    public static int createInvertedTreeDigitalInteger(int a) {
        return a % 10 * 100 + a / 10 % 10 * 10 + a / 100;
    }
}
