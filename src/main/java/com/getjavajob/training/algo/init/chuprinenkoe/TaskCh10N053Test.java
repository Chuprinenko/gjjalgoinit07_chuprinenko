package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N053.copyArrayInReversOrder;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 16.02.16.
 */
public class TaskCh10N053Test {
    public static void main(String[] args) {
        testCopyArrayInReversOrder();
    }

    public static void testCopyArrayInReversOrder() {
        int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] array2 = new int[array1.length];
        int[] arrayExpected = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh10N053Test.testCopyArrayInReversOrder", arrayExpected,
                copyArrayInReversOrder(array1, array2, 0));
    }
}
