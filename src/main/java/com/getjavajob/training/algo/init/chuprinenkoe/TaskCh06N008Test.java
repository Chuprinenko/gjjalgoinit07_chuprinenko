package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh06N008.createLimitedSequence;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 05.02.16.
 */
public class TaskCh06N008Test {
    public static void main(String[] args) {
        testLimitedSequence();
    }

    public static void testLimitedSequence() {
        int[] arrayLimitIsInteger82 = {1, 4, 9, 16, 25, 36, 49, 64, 81};
        assertEquals("TaskCh06N008Test.testLimitedSequence", arrayLimitIsInteger82, createLimitedSequence(82));
    }
}
