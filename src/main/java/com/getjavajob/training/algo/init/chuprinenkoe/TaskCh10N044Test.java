package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N044.calculateDigitalRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N044Test {
    public static void main(String[] args) {
        testDigitalRoot();
    }

    public static void testDigitalRoot() {
        assertEquals("TaskCh10N044Test.testDigitalRoot", 7, calculateDigitalRoot(55555));
    }
}
