package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N042.calculatePowerOfNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N042Test {
    public static void main(String[] args) {
        testPowerOfNumber();
    }

    public static void testPowerOfNumber() {
        assertEquals("TaskCh10N042Test.testPowerOfNumber", 0.125, calculatePowerOfNumber(0.5, 3));
    }
}
