package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh05N038.calculateDistanceFromHome;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh05N038.calculateTotalDistanceTraveled;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 04.02.16.
 */
public class TaskCh05N038Test {
    public static void main(String[] args) {
        testDistanceFromHome();
        testTotalDistanceTraveled();
    }

    public static void testDistanceFromHome() {
        assertEquals("TaskCh05N038Test.testDistanceFromHome", 0.5, calculateDistanceFromHome(1.0, 2));
    }

    public static void testTotalDistanceTraveled() {
        assertEquals("TaskCh05N038Test.testDistanceTraveled", 1.5, calculateTotalDistanceTraveled(1.0, 2));
    }
}
