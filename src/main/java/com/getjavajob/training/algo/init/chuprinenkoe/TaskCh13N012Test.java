package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 02.03.16.
 */
public class TaskCh13N012Test {
    public static void main(String[] args) {
        testCalculateYearsWorked();
        testCreateReportEmployeesWorkedYears();
        testSearchEmployees();
    }

    public static void testCalculateYearsWorked() {
        Employee id0003 = new Employee("Sergeev", "Sergey", "SPb", 3, 2010);
        assertEquals("TaskCh13N012Test.testCalculateYearsWorked", 5, id0003.calculateYearsWorked(2, 2016));
    }

    public static void testCreateReportEmployeesWorkedYears() {
        Database firstList = new Database();
        Employee id0001 = new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", 2, 2016);
        firstList.adToList(id0001);
        Employee id0002 = new Employee("Petrov", "Petr", "Petrovich", "Moscow", 2, 2014);
        firstList.adToList(id0002);
        Employee id0003 = new Employee("Sergeev", "Sergey", "SPb", 3, 2010);
        firstList.adToList(id0003);
        Employee id0004 = new Employee("Lee", "Bruce", "Shanghai", 9, 1965);
        firstList.adToList(id0004);
        Employee id0005 = new Employee("Norris", "Chuck", "Ryan", 3, 1987);
        firstList.adToList(id0005);
        Employee id0006 = new Employee("Dzu", "Konstantin", "Borisovich", "Serov", 5, 2015);
        firstList.adToList(id0006);
        Employee id0007 = new Employee("Sova", "Oleg", "SPb", 7, 1999);
        firstList.adToList(id0007);
        Employee id0008 = new Employee("Zaits", "Nina", "Lvovna", 3, 2008);
        firstList.adToList(id0008);
        Employee id0009 = new Employee("Han", "Igor", "Tashkent", 11, 2012);
        firstList.adToList(id0009);
        Employee id0010 = new Employee("Smirnov", "Sergey", "Ivanovich", "Kiev", 2, 2014);
        firstList.adToList(id0010);
        Employee id0011 = new Employee("Ilin", "Albert", "Moskow", 12, 2011);
        firstList.adToList(id0011);
        Employee id0012 = new Employee("Fedorova", "Galina", "Sergeevna", "Klen", 9, 2009);
        firstList.adToList(id0012);
        Employee id0013 = new Employee("Soev", "Sergey", "Petrovich", "Rostov", 6, 2007);
        firstList.adToList(id0013);
        Employee id0014 = new Employee("Pop", "Lev", "SPb", 2, 2014);
        firstList.adToList(id0014);
        Employee id0015 = new Employee("Karayan", "Arman", "Erevan", 5, 2002);
        firstList.adToList(id0015);
        Employee id0016 = new Employee("Karayan", "Ura", "Erevan", 2, 2005);
        firstList.adToList(id0016);
        Employee id0017 = new Employee("Gusev", "Fedor", "Moscow", 11, 2015);
        firstList.adToList(id0017);
        Employee id0018 = new Employee("Larin", "Sasha", "Olegovich", "Kiev", 1, 2010);
        firstList.adToList(id0018);
        Employee id0019 = new Employee("Markelov", "Andrei", "SPb", 2, 2016);
        firstList.adToList(id0019);
        Employee id0020 = new Employee("Stalin", "Eosiv", "Moscow", 9, 2015);
        firstList.adToList(id0020);
        Database reportExpected = new Database();
        reportExpected.adToList(id0004);
        reportExpected.adToList(id0005);
        assertEquals("TaskCh13N012Test.testCreateReportEmployeesWorkedYears", reportExpected.toString(),
                firstList.createReportEmployeesWorkedYears(2, 2016, 20).toString());
        System.out.println("reportExpected: ");
        reportExpected.printDatabaseWithoutDate();
    }

    public static void testSearchEmployees() {
        Database firstList = new Database();
        Employee id0001 = new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", 2, 2016);
        firstList.adToList(id0001);
        Employee id0002 = new Employee("Petrov", "Petr", "Petrovich", "Moscow", 2, 2014);
        firstList.adToList(id0002);
        Employee id0003 = new Employee("Sergeev", "Sergey", "SPb", 3, 2010);
        firstList.adToList(id0003);
        Employee id0004 = new Employee("Lee", "Bruce", "Shanghai", 9, 1965);
        firstList.adToList(id0004);
        Employee id0005 = new Employee("Norris", "Chuck", "Ryan", 3, 1987);
        firstList.adToList(id0005);
        Employee id0006 = new Employee("Dzu", "Konstantin", "Borisovich", "Serov", 5, 2015);
        firstList.adToList(id0006);
        Employee id0007 = new Employee("Sova", "Oleg", "SPb", 7, 1999);
        firstList.adToList(id0007);
        Employee id0008 = new Employee("Zaits", "Nina", "Lvovna", 3, 2008);
        firstList.adToList(id0008);
        Employee id0009 = new Employee("Han", "Igor", "Tashkent", 11, 2012);
        firstList.adToList(id0009);
        Employee id0010 = new Employee("Smirnov", "Sergey", "Ivanovich", "Kiev", 2, 2014);
        firstList.adToList(id0010);
        Employee id0011 = new Employee("Ilin", "Albert", "Moskow", 12, 2011);
        firstList.adToList(id0011);
        Employee id0012 = new Employee("Fedorova", "Galina", "Sergeevna", "Klen", 9, 2009);
        firstList.adToList(id0012);
        Employee id0013 = new Employee("Soev", "Sergey", "Petrovich", "Rostov", 6, 2007);
        firstList.adToList(id0013);
        Employee id0014 = new Employee("Pop", "Lev", "SPb", 2, 2014);
        firstList.adToList(id0014);
        Employee id0015 = new Employee("Karayan", "Arman", "Erevan", 5, 2002);
        firstList.adToList(id0015);
        Employee id0016 = new Employee("Karayan", "Ura", "Erevan", 2, 2005);
        firstList.adToList(id0016);
        Employee id0017 = new Employee("Gusev", "Fedor", "Moscow", 11, 2015);
        firstList.adToList(id0017);
        Employee id0018 = new Employee("Larin", "Sasha", "Olegovich", "Kiev", 1, 2010);
        firstList.adToList(id0018);
        Employee id0019 = new Employee("Markelov", "Andrei", "SPb", 2, 2016);
        firstList.adToList(id0019);
        Employee id0020 = new Employee("Stalin", "Eosiv", "Moscow", 9, 2015);
        firstList.adToList(id0020);
        Database reportExpected = new Database();
        reportExpected.adToList(id0016);
        assertEquals("TaskCh13N012Test.testSearchEmployees",
                reportExpected.toString(), firstList.searchEmployees("uR").toString());
        System.out.println("reportExpected: ");
        reportExpected.printDatabase();
    }
}
