package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh05N010.calculateSheetUsdToRub;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 04.02.16.
 */
public class TaskCh05N010Test {
    public static void main(String[] args) {
        testTabUsdToRub();
    }

    public static void testTabUsdToRub() {
        double exchangeT[][] = {
                {1.0, 50.0},
                {2.0, 100.0},
                {3.0, 150.0},
                {4.0, 200.0},
                {5.0, 250.0},
                {6.0, 300.0},
                {7.0, 350.0},
                {8.0, 400.0},
                {9.0, 450.0},
                {10.0, 500.0},
                {11.0, 550.0},
                {12.0, 600.0},
                {13.0, 650.0},
                {14.0, 700.0},
                {15.0, 750.0},
                {16.0, 800.0},
                {17.0, 850.0},
                {18.0, 900.0},
                {19.0, 950.0},
                {20.0, 1000.0}
        };
        assertEquals("TaskCh05N010Test.testTabUsdToRub", exchangeT, calculateSheetUsdToRub(50));
    }
}
