package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N050.calculationAckermann;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 13.02.16.
 */
public class TaskCh10N050Test {
    public static void main(String[] args) {
        testAckermann();
    }

    public static void testAckermann() {
        assertEquals("TaskCh10N050Test.testAckermann", 5, calculationAckermann(2, 1));
    }
}
