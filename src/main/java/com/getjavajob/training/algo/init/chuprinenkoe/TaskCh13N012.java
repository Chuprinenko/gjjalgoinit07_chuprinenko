package com.getjavajob.training.algo.init.chuprinenkoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static com.getjavajob.training.algo.util.SimpleMethods.createMonth;
import static com.getjavajob.training.algo.util.SimpleMethods.createYear;
import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 18.03.16.
 */
public class TaskCh13N012 {
    public static void main(String[] args) {
        Database firstList = new Database();
        Employee id0001 = new Employee("Ivanov", "Ivan", "Ivanovich", "Moscow", 2, 2016);
        firstList.adToList(id0001);
        firstList.printDatabase();
        System.out.println();
        Database report1 = firstList.createReportEmployeesWorkedYears(2, 2016, 0);
        report1.printDatabaseWithoutDate();
        System.out.println();
        Database report2 = firstList.searchEmployees("iV");
        report2.printDatabase();
    }
}

class Employee {
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private int monthJoining;
    private int yearOfJoining;

    public Employee() {
        Scanner in = new Scanner(System.in);
        out.println("New employee");
        out.print("Enter the surname of the employee: ");
        surname = in.nextLine();
        out.print("Enter the name of the employee: ");
        name = in.nextLine();
        out.print("Enter the patronymic of the employee: ");
        patronymic = in.nextLine();
        out.print("Enter the address of the employee: ");
        address = in.nextLine();
        out.println("Enrollment Date");
        out.print("Enter month: ");
        monthJoining = createMonth();
        out.print("Enter year: ");
        yearOfJoining = createYear();
    }

    public Employee(String surname, String name, String patronymic, String address, int monthJoining,
                    int yearOfJoining) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.monthJoining = monthJoining;
        this.yearOfJoining = yearOfJoining;
    }

    public Employee(String surname, String name, String address, int monthJoining, int yearOfJoining) {
        this(surname, name, "", address, monthJoining, yearOfJoining);
    }

    public String toString() {
        return surname + " " + name + " " + patronymic + ", " + address + ", " + monthJoining + "/" + yearOfJoining;
    }

    public int calculateYearsWorked(int reportMonth, int reportYear) {
        return ((reportYear * 12 + reportMonth) - (getYearOfJoining() * 12 + getMonthJoining())) / 12;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public int getMonthJoining() {
        return monthJoining;
    }

    public int getYearOfJoining() {
        return yearOfJoining;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMonthJoining(int monthJoining) {
        this.monthJoining = monthJoining;
    }

    public void setYearOfJoining(int yearOfJoining) {
        this.yearOfJoining = yearOfJoining;
    }

    public boolean equalsEmployee1(Employee x) {
        return Objects.equals(this.surname, x.surname) && Objects.equals(this.name, x.name) &&
                Objects.equals(this.patronymic, x.patronymic) && Objects.equals(this.address, x.address) &&
                this.monthJoining == x.monthJoining && this.yearOfJoining == x.yearOfJoining;
    }
}

class Database {
    private List<Employee> list;

    public Database() {
        list = new ArrayList<>();
    }

    public void printDatabase() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    public void printDatabaseWithoutDate() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getSurname() + " " + list.get(i).getName() + " " + list.get(i).getPatronymic() +
                    ", " + list.get(i).getAddress());
        }
    }

    public Database createReportEmployeesWorkedYears(int reportMonth, int reportYear,
                                                     int reportYearsWorked) {
        Database reportList = new Database();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).calculateYearsWorked(reportMonth, reportYear) >= reportYearsWorked) {
                reportList.getList().add(list.get(i));
            }
        }
        return reportList;
    }

    public Database searchEmployees(String searchLine) {
        Database reportList = new Database();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSurname().toLowerCase().contains(searchLine.toLowerCase()) ||
                    list.get(i).getName().toLowerCase().contains(searchLine.toLowerCase()) ||
                    list.get(i).getPatronymic().toLowerCase().contains(searchLine.toLowerCase())) {
                reportList.getList().add(list.get(i));
            }
        }
        return reportList;
    }

    public void adToList(Employee i) {
        list.add(i);
    }

    public List<Employee> getList() {
        return list;
    }

    public void setList(List<Employee> list) {
        this.list = list;
    }

    public String toString() {
        return list.toString();
    }
}
