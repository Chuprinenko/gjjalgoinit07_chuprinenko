package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N033 {
    public static void main(String[] args) {
        int naturalNumber = createNaturalNumber();
        System.out.println("Is last numeral the even? - " + isLastNumeralEven(naturalNumber));
        System.out.println("Is last numeral the odd? - " + isLastNumeralOdd(naturalNumber));
    }

    public static boolean isLastNumeralEven(int x) {
        return x % 2 == 0;
    }

    public static boolean isLastNumeralOdd(int x) {
        return x % 2 == 1;
    }
}
