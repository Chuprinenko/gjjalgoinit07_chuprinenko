package com.getjavajob.training.algo.init.chuprinenkoe;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.SimpleMethods.createPointsBasketball;
import static com.getjavajob.training.algo.util.SimpleMethods.createTeamNumber;
import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 05.02.16.
 */
public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game1 = new Game();
        serviceBasketball(game1);
    }

    // Draw in basketball is impossible
    public static void serviceBasketball(Game gameName) {
        gameName.createTeam1Name();
        gameName.createTeam2Name();
        out.println("Basketball! (Team 1) " + gameName.getTeam1Name() + " vs " + gameName.getTeam2Name() + " (Team 2)");
        int points;
        do {
            out.print("Enter the team number: ");
            int teamNumber = createTeamNumber();
            if (teamNumber == 1) {
                out.print("Enter the number of scored points by a Team 1: ");
                points = createPointsBasketball();
                gameName.setScoreTeam1(points);
                gameName.print();
            } else {
                out.print("Enter the number of scored points by a Team 2: ");
                points = createPointsBasketball();
                gameName.setScoreTeam2(points);
                gameName.print();
            }
        } while (points != 0);
        gameName.printWhichTeamWon();
    }
}

class Game {
    private int scoreTeam1;
    private int scoreTeam2;
    private String team1Name;
    private String team2Name;

    public String toString() {
        return team1Name + " (Team 1) " + scoreTeam1 + ":" + scoreTeam2 + " (Team 2) " + team2Name;
    }

    public void print() {
        System.out.println(this.toString());
    }

    public void createTeam1Name() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the name of the first team: ");
        this.team1Name = in.nextLine();
    }

    public void createTeam2Name() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the name of the second team: ");
        this.team2Name = in.nextLine();
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setScoreTeam1(int points) {
        scoreTeam1 += points;
    }

    public void setScoreTeam2(int points) {
        scoreTeam2 += points;
    }

    public void printWhichTeamWon() {
        if (scoreTeam1 > scoreTeam2) {
            System.out.println("Team 1 (" + team1Name + ") won!");
        }
        if (scoreTeam2 > scoreTeam1) {
            System.out.println("Team 2 (" + team2Name + ") won!");
        }
    }
}
