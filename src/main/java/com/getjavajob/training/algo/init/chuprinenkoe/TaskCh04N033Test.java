package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N033.isLastNumeralEven;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N033.isLastNumeralOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsLastNumeralEven();
        testIsLastNumeralOdd();
    }

    public static void testIsLastNumeralEven() {
        assertEquals("TaskCh04N033Test.testLastNumeralEven", true, isLastNumeralEven(554));
    }

    public static void testIsLastNumeralOdd() {
        assertEquals("TaskCh04N033Test.testLastNumeralOdd", true, isLastNumeralOdd(555));
    }
}
