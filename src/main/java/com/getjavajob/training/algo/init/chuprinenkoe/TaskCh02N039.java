package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.*;

/**
 * Created by chuprinenko on 29.01.16.
 */
public class TaskCh02N039 {
    public static void main(String[] args) {
        int hour = createHours();
        int minute = createMinutes();
        int second = createSeconds();
        System.out.print("The angle of rotation hour hand: " + calculateAngleOfRotationHourHand(hour, minute, second));
    }

    public static double calculateAngleOfRotationHourHand(int h, int m, int s) {
        double oneHourAngle = 720.0 / 24;
        double oneMinuteAngle = 720.0 / 24 / 60;
        double oneSecondAngle = 720.0 / 24 / 60 / 60;
        double angleOfRotationHourHand = h * oneHourAngle + m * oneMinuteAngle + s * oneSecondAngle;
        if (angleOfRotationHourHand >= 360) {
            angleOfRotationHourHand -= 360;
        }
        return angleOfRotationHourHand;
    }
}
