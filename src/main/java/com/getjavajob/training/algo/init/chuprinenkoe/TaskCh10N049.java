package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N049 {
    public static void main(String[] args) {
        System.out.println("Index of max element of the array is " +
                calculateIndexOfMaxElementInArray(createArray(), 0));
    }

    public static int calculateIndexOfMaxElementInArray(int[] array, int i) {
        if (i < array.length) {
            int next = calculateIndexOfMaxElementInArray(array, i + 1);
            return (array[i] > array[next]) ? i : next;
        } else {
            return 0;
        }
    }
}
