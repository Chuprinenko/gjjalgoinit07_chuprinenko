package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createYear;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N115 {
    public static void main(String[] args) {
        int year = createYear();
        System.out.print("Year #" + year + " - " + calculateNameOfYear(year) + ", " + calculateColorOfYear(year));
    }

    public static String calculateNameOfYear(int year) {
        String name = "";
        int nameLoop = year % 12;
        switch (nameLoop) {
            case 0:
                name = "Monkey";
                break;
            case 1:
                name = "Rooster";
                break;
            case 2:
                name = "Dog";
                break;
            case 3:
                name = "Pig";
                break;
            case 4:
                name = "Rat";
                break;
            case 5:
                name = "Ox";
                break;
            case 6:
                name = "Tiger";
                break;
            case 7:
                name = "Rabbit";
                break;
            case 8:
                name = "Dragon";
                break;
            case 9:
                name = "Snake";
                break;
            case 10:
                name = "Horse";
                break;
            case 11:
                name = "Ram";
                break;
        }
        return name;
    }

    public static String calculateColorOfYear(int year) {
        String color = "";
        int colorLoop = year % 10;
        switch (colorLoop) {
            case 0:
            case 1:
                color = "White";
                break;
            case 2:
            case 3:
                color = "Black";
                break;
            case 4:
            case 5:
                color = "Green";
                break;
            case 6:
            case 7:
                color = "Red";
                break;
            case 8:
            case 9:
                color = "Yellow";
                break;
        }
        return color;
    }
}
