package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N048.calculateMaxElementInArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N048Test {
    public static void main(String[] args) {
        testMaxElementInArray();
    }

    public static void testMaxElementInArray() {
        int[] array = {1, 2, 3, 4, 5, 6, 5, 4, 3, 2};
        assertEquals("TaskCh10N048Test.testMaxElementInArray", 6, calculateMaxElementInArray(array, 0));
    }
}
