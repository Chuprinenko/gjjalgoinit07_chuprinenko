package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createWord;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.print(createReverseWord(createWord()));
    }

    public static String createReverseWord(String word) {
        StringBuilder reversWord = new StringBuilder(word);
        return reversWord.reverse().toString();
    }
}