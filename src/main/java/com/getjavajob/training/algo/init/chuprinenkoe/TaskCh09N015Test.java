package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N015.takeCharacterFromWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N015Test {
    public static void main(String[] args) {
        testCharFromWord();
    }

    public static void testCharFromWord() {
        assertEquals("TaskCh09N015Test.testCharFromWord", 'A', takeCharacterFromWord("MAMA", 1));
    }
}
