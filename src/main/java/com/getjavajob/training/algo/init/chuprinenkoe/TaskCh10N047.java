package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.println("Enter the number of term of the Fibonacci sequence (n). ");
        System.out.print(calculateTermOfFibonacciSequence(createNaturalNumber()));
    }

    public static int calculateTermOfFibonacciSequence(int numberOfTerm) {
        if (numberOfTerm == 1 || numberOfTerm == 2) {
            return 1;
        }
        return calculateTermOfFibonacciSequence(numberOfTerm - 2) + calculateTermOfFibonacciSequence(numberOfTerm - 1);
    }
}