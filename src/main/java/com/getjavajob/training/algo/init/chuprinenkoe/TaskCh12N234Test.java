package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N234.deleteRow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 27.02.16.
 */
public class TaskCh12N234Test {
    public static void main(String[] args) {
        test1DeleteColumn();
        test2DeleteColumn();
        test1DeleteRow();
        test2DeleteRow();
    }

    public static void test1DeleteColumn() {
        int[][] array = {
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5}
        };
        int[][] result = {
                {1, 2, 4, 5, 0},
                {1, 2, 4, 5, 0},
                {1, 2, 4, 5, 0},
                {1, 2, 4, 5, 0},
                {1, 2, 4, 5, 0}
        };
        assertEquals("TaskCh12N234Test.test1DeleteColumn", result, deleteColumn(array, 2));
    }

    public static void test2DeleteColumn() {
        int[][] array = {
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5}
        };
        int[][] result = {
                {1, 2, 3, 4, 0},
                {1, 2, 3, 4, 0},
                {1, 2, 3, 4, 0},
                {1, 2, 3, 4, 0},
                {1, 2, 3, 4, 0}
        };
        assertEquals("TaskCh12N234Test.test2DeleteColumn", result, deleteColumn(array, 4));
    }

    public static void test1DeleteRow() {
        int[][] array = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5}
        };
        int[][] result = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5},
                {0, 0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.test1DeleteRow", result, deleteRow(array, 2));
    }

    public static void test2DeleteRow() {
        int[][] array = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5}
        };
        int[][] result = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4},
                {0, 0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.test2DeleteRow", result, deleteRow(array, 4));
    }
}
