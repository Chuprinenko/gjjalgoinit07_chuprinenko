package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N045.calculateSumOfAP;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N045.calculateTermOfAP;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N045Test {
    public static void main(String[] args) {
        testTermOfAP();
        testSumOfAP();
    }

    public static void testTermOfAP() {
        assertEquals("TaskCh10N045Test.testTermOfAP", 45, calculateTermOfAP(10, 5, 8));
    }

    public static void testSumOfAP() {
        assertEquals("TaskCh10N045Test.testSumOfAP", 15, calculateSumOfAP(1, 1, 5));
    }
}