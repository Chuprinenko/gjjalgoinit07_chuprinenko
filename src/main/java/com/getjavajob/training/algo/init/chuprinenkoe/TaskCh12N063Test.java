package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N063.calculateAverageNumberOfStudentsAtEachLevel;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 27.02.16.
 */
public class TaskCh12N063Test {
    public static void main(String[] args) {
        testCalculateAverageNumberOfStudentsAtEachLevel();
    }

    public static void testCalculateAverageNumberOfStudentsAtEachLevel() {
        int[][] school = {
                {10, 20, 30, 40},
                {10, 11, 11, 10},
                {10, 12, 20, 10},
                {10, 12, 12, 13},
                {10, 10, 10, 10},
                {10, 20, 10, 20},
                {10, 12, 10, 12},
                {11, 13, 11, 13},
                {13, 15, 13, 15},
                {10, 11, 12, 13},
                {10, 15, 20, 25}
        };
        double[] studentsInLevels = {25, 10.5, 13, 11.75, 10, 15, 11, 12, 14, 11.5, 17.5};
        assertEquals("TaskCh12N063Test.testCalculateAverageNumberOfStudentsAtEachLevel", studentsInLevels,
                calculateAverageNumberOfStudentsAtEachLevel(school));
    }
}
