package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;
import static java.lang.Math.*;
import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 27.01.16.
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        printAndCalculateExpression1();
        out.println();
        printAndCalculateExpression2();
        out.println();
        printAndCalculateExpression3();
        out.println();
        printAndCalculateExpression4();
    }

    public static void printAndCalculateExpression1() {
        out.println("Expression #1: sqrt(1 - pow(sin(x), 2))");
        out.print("Enter a variable x: ");
        double x = createDouble();
        double result = sqrt(1 - pow(sin(x), 2));
        out.println("sqrt(1 - pow(sin(x), 2)) = " + result);
    }

    public static void printAndCalculateExpression2() {
        out.println("Expression #2: 1 / (sqrt(a*pow(x, 2) + b*x + c))");
        out.println("Condition: a*pow(x, 2) + b*x + c >= 0");
        double a;
        double b;
        double c;
        double x;
        do {
            out.print("Enter a variable a: ");
            a = createDouble();
            out.print("Enter a variable b: ");
            b = createDouble();
            out.print("Enter a variable c: ");
            c = createDouble();
            out.print("Enter a variable x: ");
            x = createDouble();
            if ((a * pow(x, 2) + b * x + c) < 0) {
                out.println("Condition isn't satisfied. Enter the variables again");
            }
        } while ((a * pow(x, 2) + b * x + c) < 0);
        double result = 1 / (sqrt(a * pow(x, 2) + b * x + c));
        out.println("1 / (sqrt(a*pow(x, 2) + b*x + c)) = " + result);
    }

    public static void printAndCalculateExpression3() {
        out.println("Expression #3: (sqrt(x + 1) + sqrt(x - 1)) / (2*sqrt(x))");
        out.println("Condition: x >= 1");
        double x;
        do {
            out.print("Enter a variable x: ");
            x = createDouble();
            if (x < 1) {
                out.println("Condition isn't satisfied. Enter the variable again");
            }
        } while (x < 1);
        double result = (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
        out.println("(sqrt(x + 1) + sqrt(x - 1)) / (2*sqrt(x)) = " + result);
    }

    public static void printAndCalculateExpression4() {
        out.println("Expression #4: abs(x) + abs(x + 1)");
        out.print("Enter a variable x: ");
        double x;
        x = createDouble();
        double result = abs(x) + abs(x + 1);
        out.println("abs(x) + abs(x + 1) = " + result);
    }
}
