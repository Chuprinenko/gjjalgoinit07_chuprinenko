package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N115.calculateColorOfYear;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh04N115.calculateNameOfYear;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N115Test {
    public static void main(String[] args) {
        test1NameOfYear();
        test1ColorOfYear();
        test2NameOfYear();
        test2ColorOfYear();
    }

    public static void test1NameOfYear() {
        assertEquals("TaskCh04N115Test.test1NameOfYear", "Rat", calculateNameOfYear(1984));
    }

    public static void test1ColorOfYear() {
        assertEquals("TaskCh04N115Test.test1ColorOfYear", "Green", calculateColorOfYear(1984));
    }

    public static void test2NameOfYear() {
        assertEquals("TaskCh04N115Test.test2NameOfYear", "Monkey", calculateNameOfYear(2016));
    }

    public static void test2ColorOfYear() {
        assertEquals("TaskCh04N115Test.test2ColorOfYear", "Red", calculateColorOfYear(2016));
    }
}
