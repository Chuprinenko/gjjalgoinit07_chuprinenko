package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 24.02.16.
 */
public class TaskCh12N023 {
    public static void main(String[] args) {
        System.out.println("Array A");
        int[][] arrayA = createArrayTaskA(7, 7);
        printArray(arrayA);
        System.out.println("Array B");
        int[][] arrayB = createArrayTaskB(7, 7);
        printArray(arrayB);
        System.out.println("Array C");
        int[][] arrayC = createArrayTaskC(7, 7);
        printArray(arrayC);
    }

    public static int[][] createArrayTaskA(int lines, int columns) {
        int[][] array = new int[lines][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j || i + j == array.length - 1) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = 0;
                }
            }
        }
        return array;
    }

    public static int[][] createArrayTaskB(int lines, int columns) {
        int[][] array = new int[lines][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j || i + j == array.length - 1 || i == array.length / 2 || j == array.length / 2) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = 0;
                }
            }
        }
        return array;
    }

    public static int[][] createArrayTaskC(int lines, int columns) {
        int[][] array = new int[lines][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i - j > 0 && i + j < array.length - 1 || i - j < 0 && i + j > array.length - 1) {
                    array[i][j] = 0;
                } else {
                    array[i][j] = 1;
                }
            }
        }
        return array;
    }
}
