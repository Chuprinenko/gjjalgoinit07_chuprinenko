package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNotNegativeInteger;

/**
 * Created by chuprinenkoe on 13.02.16.
 */
public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.println("Ackermann function: A(m, n)");
        System.out.print("Enter the integer m. Condition: m >= 0: ");
        int m = createNotNegativeInteger();
        System.out.print("Enter the integer n. Condition: n >= 0: ");
        int n = createNotNegativeInteger();
        System.out.print("A(" + m + ", " + n + ") = " + calculationAckermann(m, n));
    }

    public static int calculationAckermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        }
        if (n == 0) {
            return calculationAckermann(m - 1, 1);
        } else {
            return calculationAckermann(m - 1, calculationAckermann(m, n - 1));
        }
    }
}