package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh02N031.swapSecondAndThirdDigitsInThreeDigitalInteger;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by  chuprinenkoe on 29.01.16.
 */
public class TaskCh02N031Test {
    public static void main(String[] args) {
        testSwapSecondAndThirdDigitsInThreeDigitalNumber();
    }

    public static void testSwapSecondAndThirdDigitsInThreeDigitalNumber() {
        assertEquals("TaskCh02N031Test.testSwapSecondAndThirdDigitsInThreeDigitalInteger", 521,
                swapSecondAndThirdDigitsInThreeDigitalInteger(512));
    }
}
