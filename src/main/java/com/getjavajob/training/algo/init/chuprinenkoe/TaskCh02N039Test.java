package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh02N039.calculateAngleOfRotationHourHand;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 29.01.16.
 */
public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngleOfRotationHourHand();
    }

    public static void testAngleOfRotationHourHand() {
        assertEquals("TaskCh02N039Test.testCalculateAngleOfRotationHourHand", 45.0,
                calculateAngleOfRotationHourHand(1, 30, 0));
    }
}
