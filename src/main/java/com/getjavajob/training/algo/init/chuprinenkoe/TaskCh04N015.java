package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createMonth;
import static com.getjavajob.training.algo.util.SimpleMethods.createYear;

/**
 * Created by chuprinenkoe on 02.02.16.
 */
public class TaskCh04N015 {
    public static void main(String[] args) {
        System.out.println("Enter the date of birth");
        int monthBirth = createMonth();
        int yearBirth = createYear();
        System.out.println("Enter the current date");
        int monthCurrent = createMonth();
        int yearCurrent = createYear();
        System.out.print("Age: " + calculateAgeInYears(monthBirth, yearBirth, monthCurrent, yearCurrent));
    }

    public static int calculateAgeInYears(int monthBirth, int yearBirth, int monthCurrent, int yearCurrent) {
        return monthCurrent - monthBirth < 0 ? yearCurrent - yearBirth - 1 : yearCurrent - yearBirth;
    }
}
