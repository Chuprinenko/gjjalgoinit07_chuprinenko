package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N028.createHelixInSquareArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 26.02.16.
 */
public class TaskCh12N028Test {
    public static void main(String[] args) {
        testCreateHelixInSquareArray();
    }

    public static void testCreateHelixInSquareArray() {
        int[][] array = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("TaskCh12N028Test.testCreateHelixInSquareArray", array, createHelixInSquareArray(5));
    }
}
