package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N042.createReverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 09.02.16.
 */
public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseWord();
    }

    public static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "mama", createReverseWord("amam"));
    }
}
