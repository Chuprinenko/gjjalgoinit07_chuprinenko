package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N036 {
    public static void main(String[] args) {
        System.out.print("Enter a time in minutes: ");
        System.out.print(calculateColorTrafficLight(createDouble()));
    }

    public static String calculateColorTrafficLight(double timeMinutes) {
        return timeMinutes % 5 < 3 ? "green" : "red";
    }
}
