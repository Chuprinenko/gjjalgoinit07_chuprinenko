package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N051.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 15.02.16.
 */
public class TaskCh10N051Test {
    public static void main(String[] args) {
        testTaskA();
        testTaskB();
        testTaskC();
    }

    public static void testTaskA() {
        StringBuilder a = new StringBuilder();
        assertEquals("TaskCh10N051Test.testTaskA", "54321", createTaskA(a, 5));
    }

    public static void testTaskB() {
        StringBuilder a = new StringBuilder();
        assertEquals("TaskCh10N051Test.testTaskB", "12345", createTaskB(a, 5));
    }

    public static void testTaskC() {
        StringBuilder a = new StringBuilder();
        assertEquals("TaskCh10N051Test.testTaskC", "5432112345", createTaskC(a, 5));
    }
}
