package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 24.02.16.
 */
public class TaskCh12N023Test {
    public static void main(String[] args) {
        testCreateArrayTaskA();
        testCreateArrayTaskB();
        testCreateArrayTaskC();
    }

    public static void testCreateArrayTaskA() {
        int[][] array = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testCreateArrayTaskA", array, createArrayTaskA(7, 7));
    }

    public static void testCreateArrayTaskB() {
        int[][] array = {
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testCreateArrayTaskB", array, createArrayTaskB(7, 7));
    }

    public static void testCreateArrayTaskC() {
        int[][] array = {
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("TaskCh12N023Test.testCreateArrayTaskC", array, createArrayTaskC(7, 7));
    }
}
