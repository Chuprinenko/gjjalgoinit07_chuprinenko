package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenekoe on 01.02.16.
 */
public class TaskCh03N029Test {
    public static void main(String[] args) {
        testBothIntegersOddTestOddOdd();
        testBothIntegersOddTestOddEven();
        testBothIntegersOddTestEvenOdd();
        testBothIntegersOddTestEvenEven();
        System.out.println();
        testOnlyOneIntegerLess20TestL20M20();
        testOnlyOneIntegerLess20TestL20L20();
        testOnlyOneIntegerLess20TestM20M20();
        testOnlyOneIntegerLess20TestM20L20();
        testOnlyOneIntegerLess20TestE20L20();
        testOnlyOneIntegerLess20TestL20E20();
        testOnlyOneIntegerLess20TestE20M20();
        testOnlyOneIntegerLess20TestM20E20();
        System.out.println();
        testAtLeastOneIntegerZeroTest1zero2no();
        testAtLeastOneIntegerZeroTest1no2zero();
        testAtLeastOneIntegerZeroTest1zero2zero();
        testAtLeastOneIntegerZeroTest1no2no();
        System.out.println();
        testEachIntegersNegativeTestNNN();
        testEachIntegersNegativeTestNNP();
        testEachIntegersNegativeTestNPP();
        testEachIntegersNegativeTestPPP();
        System.out.println();
        testOnlyOneIntegerMultiple5Test555();
        testOnlyOneIntegerMultiple5Test55N();
        testOnlyOneIntegerMultiple5Test5NN();
        testOnlyOneIntegerMultiple5TestNNN();
        System.out.println();
        testAtLeastOneIntegerGreater100TestGGG();
        testAtLeastOneIntegerGreater100TestGGL();
        testAtLeastOneIntegerGreater100TestGLL();
        testAtLeastOneIntegerGreater100TestLLL();
    }

    public static void testBothIntegersOddTestOddOdd() {
        assertEquals("TaskCh03N029Test.testBothIntegersOddTestOddOdd", true, isBothIntegersOdd(27, 3));
    }

    public static void testBothIntegersOddTestOddEven() {
        assertEquals("TaskCh03N029Test.testBothIntegersOddTestOddEven", false, isBothIntegersOdd(27, 4));
    }

    public static void testBothIntegersOddTestEvenOdd() {
        assertEquals("TaskCh03N029Test.testBothIntegersOddTestEvenOdd", false, isBothIntegersOdd(88, 1));
    }

    public static void testBothIntegersOddTestEvenEven() {
        assertEquals("TaskCh03N029Test.testBothIntegersOddTestEvenEven", false, isBothIntegersOdd(100, 2));
    }

    public static void testOnlyOneIntegerLess20TestL20M20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestL20M20", true, isOnlyOneIntegerLess20(19, 21));
    }

    public static void testOnlyOneIntegerLess20TestL20L20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestL20L20", false, isOnlyOneIntegerLess20(19, -5));
    }

    public static void testOnlyOneIntegerLess20TestM20M20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestM20M20", false, isOnlyOneIntegerLess20(28, 189));
    }

    public static void testOnlyOneIntegerLess20TestM20L20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestM20L20", true, isOnlyOneIntegerLess20(3098, -144));
    }

    public static void testOnlyOneIntegerLess20TestE20L20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestE20L20", true, isOnlyOneIntegerLess20(20, -144));
    }

    public static void testOnlyOneIntegerLess20TestL20E20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestL20E20", true, isOnlyOneIntegerLess20(3, 20));
    }

    public static void testOnlyOneIntegerLess20TestE20M20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestE20M20", false, isOnlyOneIntegerLess20(20, 53));
    }

    public static void testOnlyOneIntegerLess20TestM20E20() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerLess20TestM20E20", false, isOnlyOneIntegerLess20(311, 20));
    }

    public static void testAtLeastOneIntegerZeroTest1zero2no() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerZeroTest1zero2no", true, isAtLeastOneIntegerZero(0, 544));
    }

    public static void testAtLeastOneIntegerZeroTest1no2zero() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerZeroTest1no2zero", true, isAtLeastOneIntegerZero(1, 0));
    }

    public static void testAtLeastOneIntegerZeroTest1zero2zero() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerZeroTest1zero2zero", true, isAtLeastOneIntegerZero(0, 0));
    }

    public static void testAtLeastOneIntegerZeroTest1no2no() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerZeroTest1no2no", false, isAtLeastOneIntegerZero(-87, -3));
    }

    public static void testEachIntegersNegativeTestNNN() {
        assertEquals("TaskCh03N029Test.EachIntegersNegativeTestNNN", true, isEachIntegersNegative(-8, -3, -567));
    }

    public static void testEachIntegersNegativeTestNNP() {
        assertEquals("TaskCh03N029Test.EachIntegersNegativeTestNNP", false, isEachIntegersNegative(-8, -3, 33));
    }

    public static void testEachIntegersNegativeTestNPP() {
        assertEquals("TaskCh03N029Test.EachIntegersNegativeTestNNP", false, isEachIntegersNegative(-8, 777, 31));
    }

    public static void testEachIntegersNegativeTestPPP() {
        assertEquals("TaskCh03N029Test.EachIntegersNegativeTestPPP", false, isEachIntegersNegative(9732, 1, 72));
    }

    public static void testOnlyOneIntegerMultiple5Test555() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerMultiple5Test555", false,
                isOnlyOneIntegerMultiple5(5, 25, 125));
    }

    public static void testOnlyOneIntegerMultiple5Test55N() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerMultiple5Test55N", false,
                isOnlyOneIntegerMultiple5(15, 35, 7));
    }

    public static void testOnlyOneIntegerMultiple5Test5NN() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerMultiple5Test5NN", true,
                isOnlyOneIntegerMultiple5(5, 1, 11));
    }

    public static void testOnlyOneIntegerMultiple5TestNNN() {
        assertEquals("TaskCh03N029Test.testOnlyOneIntegerMultiple5TestNNN", false,
                isOnlyOneIntegerMultiple5(-3, -17, 111));
    }

    public static void testAtLeastOneIntegerGreater100TestGGG() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerGreater100TestGGG", true,
                isAtLeastOneIntegerGreater100(101, 341, 876));
    }

    public static void testAtLeastOneIntegerGreater100TestGGL() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerGreater100TestGGL", true,
                isAtLeastOneIntegerGreater100(111, 555, 99));
    }

    public static void testAtLeastOneIntegerGreater100TestGLL() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerGreater100TestGLL", true,
                isAtLeastOneIntegerGreater100(991, -5, 65));
    }

    public static void testAtLeastOneIntegerGreater100TestLLL() {
        assertEquals("TaskCh03N029Test.testAtLeastOneIntegerGreater100TestLLL", false,
                isAtLeastOneIntegerGreater100(1, 3, 8));
    }
}
