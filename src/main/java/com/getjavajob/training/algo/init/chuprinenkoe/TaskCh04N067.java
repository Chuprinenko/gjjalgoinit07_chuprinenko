package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDay;

/**
 * Created by chuprinenkoe on 03.02.16.
 */
public class TaskCh04N067 {
    public static void main(String[] args) {
        System.out.print(calculateWhatDay(createDay()));
    }

    public static String calculateWhatDay(int day) {
        return day % 7 == 0 || day % 7 == 6 ? "Weekend" : "Workday";
    }
}
