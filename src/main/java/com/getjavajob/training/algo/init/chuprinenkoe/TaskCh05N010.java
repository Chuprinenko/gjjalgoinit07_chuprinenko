package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;

/**
 * Created by chuprinenkoe on 04.02.16.
 */
public class TaskCh05N010 {
    public static void main(String[] args) {
        System.out.print("Enter USD to RUB exchange rate: ");
        printExchangeSheetUsdToRub(calculateSheetUsdToRub(createDouble()));
    }

    public static double[][] calculateSheetUsdToRub(double rate) {
        double[][] exchangeTab = new double[20][2];
        for (int i = 0; i < exchangeTab.length; i++) {
            for (int j = 0; j < exchangeTab[i].length; j++) {
                if (j == 0) {
                    exchangeTab[i][j] = i + 1;
                } else {
                    exchangeTab[i][j] = exchangeTab[i][j - 1] * rate;
                }
            }
        }
        return exchangeTab;
    }

    public static void printExchangeSheetUsdToRub(double[][] exchangeTab) {
        for (double[] lineExchangeTab : exchangeTab) {
            for (int j = 0; j < lineExchangeTab.length; j++) {
                if (j == 0) {
                    int usd = (int) lineExchangeTab[j];
                    System.out.print("$ " + usd + " \t");
                } else System.out.printf("RUB %.2f\n", lineExchangeTab[j]);
            }
        }
    }
}
