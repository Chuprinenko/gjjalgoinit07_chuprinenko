package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N043.calculateAmountOfDigits;
import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N043.calculateSumOfDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N043Test {
    public static void main(String[] args) {
        testSumOfDigits();
        testAmountOfDigits();
    }

    public static void testSumOfDigits() {
        assertEquals("TaskCh10N043Test.testSumOfDigits", 21, calculateSumOfDigits(123456));
    }

    public static void testAmountOfDigits() {
        assertEquals("TaskCh10N043Test.testAmountOfDigits", 6, calculateAmountOfDigits(123456));
    }
}
