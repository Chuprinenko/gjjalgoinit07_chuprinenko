package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N107.changeFirstAToLastO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 09.02.16.
 */
public class TaskCh09N107Test {
    public static void main(String[] args) {
        test1ChangeFirstAToLastO();
        test2ChangeFirstAToLastO();
    }

    public static void test1ChangeFirstAToLastO() {
        assertEquals("TaskCh09N107Test.test1ChangeFirstAToLastO", "oerAspace", changeFirstAToLastO("Aerospace"));
    }

    public static void test2ChangeFirstAToLastO() {
        assertEquals("TaskCh09N107Test.test2ChangeFirstAToLastO", "father", changeFirstAToLastO("father"));
    }
}
