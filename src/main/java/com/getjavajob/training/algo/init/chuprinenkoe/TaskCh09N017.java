package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createWord;

/**
 * Created by chuprinenkoe on 08.02.16.
 */
public class TaskCh09N017 {
    public static void main(String[] args) {
        String word = createWord();
        System.out.print("The first and last letter of the word is the same: " + isSameFirstAndLastLetters(word));
    }

    public static boolean isSameFirstAndLastLetters(String word) {
        return word.toLowerCase().charAt(0) == word.toLowerCase().charAt(word.length() - 1);
    }
}
