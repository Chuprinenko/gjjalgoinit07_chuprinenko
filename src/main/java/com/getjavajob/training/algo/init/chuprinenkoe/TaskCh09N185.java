package com.getjavajob.training.algo.init.chuprinenkoe;

import java.util.Scanner;

/**
 * Created by chuprinenkoe on 10.02.16.
 */
public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter а Math Expression: ");
        System.out.print(createReportAboutBrackets(in.nextLine()));
    }

    public static String createReportAboutBrackets(String expression) {
        StringBuilder report = new StringBuilder("");
        int counter = 0;
        int indexFirstExtraRight = 0;
        for (int i = 0; i < expression.length(); i++) {
            char x = expression.charAt(i);
            if (x == '(') {
                counter++;
            } else if (x == ')') {
                counter--;
                if (counter < 0) {
                    indexFirstExtraRight = i;
                    break;
                }
            }
        }
        if (counter == 0) {
            report.append("Yes");
        } else if (counter < 0) {
            report.append("No. Index of first right extra bracket: ").append(indexFirstExtraRight);
        } else {
            report.append("No. Quantity of redundant additional left brackets: ").append(counter);
        }
        return report.toString();
    }
}