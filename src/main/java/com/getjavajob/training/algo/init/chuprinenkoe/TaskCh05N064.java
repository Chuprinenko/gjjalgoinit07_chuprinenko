package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createDouble;
import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;

/**
 * Created by chuprinenkoe on 04.02.16.
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        System.out.printf("The average population density: %.2f",
                calculateAveragePopulationDensity(createSheetOfPopulation()));
        System.out.print(" (K) people per square km");
    }

    public static double[][] createSheetOfPopulation() {
        double[][] regionInfo = new double[12][2];
        for (int i = 0; i < regionInfo.length; i++) {
            for (int j = 0; j < regionInfo[i].length; j++) {
                if (j == 0) {
                    System.out.print("Enter the quantity of the population in district #" + (i + 1) + " (K): ");
                    regionInfo[i][j] = createInteger();
                } else {
                    System.out.print("Enter the area of the district #" + (i + 1) + " (square km): ");
                    regionInfo[i][j] = createDouble();
                }
            }
        }
        return regionInfo;
    }

    public static double calculateAveragePopulationDensity(double[][] sheetOfPopulation) {
        double totalPopulation = 0;
        double totalArea = 0;
        for (double[] lineSheetOfPopulation : sheetOfPopulation) {
            totalPopulation += lineSheetOfPopulation[0];
            totalArea += lineSheetOfPopulation[1];
        }
        return totalPopulation / totalArea;
    }
}
