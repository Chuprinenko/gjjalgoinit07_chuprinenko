package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static java.lang.System.out;

/**
 * Created by chuprinenkoe on 01.02.16.
 */
public class TaskCh03N029 {
    public static void main(String[] args) {
        out.print("Enter an  integer x: ");
        int x = createInteger();
        out.print("Enter an  integer y: ");
        int y = createInteger();
        out.print("Enter an  integer z: ");
        int z = createInteger();
        out.println(isBothIntegersOdd(x, y));
        out.println(isOnlyOneIntegerLess20(x, y));
        out.println(isAtLeastOneIntegerZero(x, y));
        out.println(isEachIntegersNegative(x, y, z));
        out.println(isOnlyOneIntegerMultiple5(x, y, z));
        out.println(isAtLeastOneIntegerGreater100(x, y, z));
    }

    /**
     * This method checks that each integer (x, y) is odd
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @return Value type boolean
     */
    public static boolean isBothIntegersOdd(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * This method checks that only one integer (x, y) smaller than 20
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @return Value type boolean
     */
    public static boolean isOnlyOneIntegerLess20(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * This method checks that have at least one integer (x, y) is zero
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @return Value type boolean
     */
    public static boolean isAtLeastOneIntegerZero(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * This method checks to each of the integers (x, y, z) is negative
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @param z The value of that method checks
     * @return Value type boolean
     */
    public static boolean isEachIntegersNegative(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * This method checks that only one of the integers (x, y, z) a multiple of 5
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @param z The value of that method checks
     * @return Value type boolean
     */
    public static boolean isOnlyOneIntegerMultiple5(int x, int y, int z) {
        boolean a = x % 5 == 0;
        boolean b = y % 5 == 0;
        boolean c = z % 5 == 0;
        return (a && !b && !c) || (!a && b && !c) || (!a && !b && c);
    }

    /**
     * This method checks that have at least one integer (x, y, z) greater than 100
     *
     * @param x The value of that method checks
     * @param y The value of that method checks
     * @param z The value of that method checks
     * @return Value type boolean
     */
    public static boolean isAtLeastOneIntegerGreater100(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
