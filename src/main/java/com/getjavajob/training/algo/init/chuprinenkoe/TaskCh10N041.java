package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createNaturalNumber;

/**
 * Created by chuprinenkoe on 11.02.16.
 */
public class TaskCh10N041 {
    public static void main(String[] args) {
        int number = createNaturalNumber();
        System.out.print("Factorial of " + number + " is " + calculateFactorial(number));
    }

    public static int calculateFactorial(int naturalNumber) {
        if (naturalNumber == 1) {
            return 1;
        }
        return calculateFactorial(naturalNumber - 1) * naturalNumber;
    }
}
