package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createArray;

/**
 * Created by chuprinenkoe on 12.02.16.
 */
public class TaskCh10N048 {
    public static void main(String[] args) {
        System.out.print("Max element of the array is " + calculateMaxElementInArray(createArray(), 0));
    }

    public static int calculateMaxElementInArray(int[] array, int numberOfInitialElement) {
        if (numberOfInitialElement < array.length) {
            int next = calculateMaxElementInArray(array, numberOfInitialElement + 1);
            return (array[numberOfInitialElement] > next) ? array[numberOfInitialElement] : next;
        } else {
            return array[numberOfInitialElement - array.length];
        }
    }
}
