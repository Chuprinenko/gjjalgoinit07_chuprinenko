package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh09N185.createReportAboutBrackets;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 10.02.16.
 */
public class TaskCh09N185Test {
    public static void main(String[] args) {
        test1ReportAboutBrackets();
        test2ReportAboutBrackets();
        test3ReportAboutBrackets();
    }

    public static void test1ReportAboutBrackets() {
        assertEquals("TaskCh09N185Test.test1ReportAboutBrackets", "Yes", createReportAboutBrackets("(())()"));
    }

    public static void test2ReportAboutBrackets() {
        assertEquals("TaskCh09N185Test.test2ReportAboutBrackets", "No. Index of first right extra bracket: 0",
                createReportAboutBrackets(")(())("));
    }

    public static void test3ReportAboutBrackets() {
        assertEquals("TaskCh09N185Test.test3ReportAboutBrackets",
                "No. Quantity of redundant additional left brackets: 5", createReportAboutBrackets("((((("));
    }
}
