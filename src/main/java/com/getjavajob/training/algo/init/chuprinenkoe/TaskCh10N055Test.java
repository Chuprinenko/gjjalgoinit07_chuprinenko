package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.init.chuprinenkoe.TaskCh10N055.conversionIntegerInAnotherNumeralSystem;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by chuprinenkoe on 16.02.16.
 */
public class TaskCh10N055Test {
    public static void main(String[] args) {
        testIntegerInAnotherNumeralSystem();
    }

    public static void testIntegerInAnotherNumeralSystem() {
        StringBuilder result = new StringBuilder();
        assertEquals("TaskCh10N055Test.testIntegerInAnotherNumeralSystem", "1111",
                conversionIntegerInAnotherNumeralSystem(15, 2, result));
    }
}
