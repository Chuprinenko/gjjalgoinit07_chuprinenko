package com.getjavajob.training.algo.init.chuprinenkoe;

import static com.getjavajob.training.algo.util.SimpleMethods.createInteger;
import static com.getjavajob.training.algo.util.SimpleMethods.printArray;

/**
 * Created by chuprinenkoe on 26.02.16.
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        System.out.println("Helix Array");
        System.out.print("Enter a side of a square array (n): ");
        int[][] array = createHelixInSquareArray(createInteger());
        printArray(array);
    }

    public static int[][] createHelixInSquareArray(int n) {
        int[][] array = new int[n][n];
        int arrayVolume = n * n;
        int line = 0;
        int column = 0;
        int lineDirection = 0;
        int columnDirection = 1;
        int route = 0;
        int steps = n;
        for (int i = 1; i <= arrayVolume; i++) {
            array[line][column] = i;
            steps--;
            if (steps == 0) {
                steps = n - 1 - route / 2;
                int temp = columnDirection;
                columnDirection = -lineDirection;
                lineDirection = temp;
                route++;
            }
            line += lineDirection;
            column += columnDirection;
        }
        return array;
    }
}
